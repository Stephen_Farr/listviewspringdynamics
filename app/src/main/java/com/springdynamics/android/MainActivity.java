package com.springdynamics.android;

import android.content.res.Resources;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.rebound.BaseSpringSystem;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringChain;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;
import com.springdynamics.android.springlistview.SpringAdapter;
import com.springdynamics.android.springlistview.SpringRecyclerView;
import com.springdynamics.android.views.SpringView;


public class MainActivity extends ActionBarActivity {
    private SpringChain springChain;
    private boolean wasScrolling;
    private float original;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources r = getResources();
        original = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, r.getDisplayMetrics());

        springChain = SpringChain.create();

        SpringRecyclerView springRecyclerView = (SpringRecyclerView)findViewById(R.id.recycler_view);
        springRecyclerView.setAdapter(new SpringAdapter(springChain));
        springRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        springRecyclerView.setHasFixedSize(true);
        springRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager manager = (LinearLayoutManager)recyclerView.getLayoutManager();
                springChain.setControlSpringIndex(manager.findFirstCompletelyVisibleItemPosition());
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        wasScrolling = true;
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        wasScrolling = true;
                        springChain.getControlSpring().setEndValue(original - 10);
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (wasScrolling) {
                            springChain.getControlSpring().setEndValue(original);
                            wasScrolling = false;
                        }
                        break;
                }
            }
        });
    }
}
