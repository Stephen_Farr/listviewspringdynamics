package com.springdynamics.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.springdynamics.android.R;

/**
 * Created by Stephen on 3/10/2015.
 */
public class SpringView extends RelativeLayout implements SpringListener {

    public View middleView;

    public SpringView(Context context) {
        super(context);

        init(context);
    }

    public SpringView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.spring_view, null, false);
        middleView = view.findViewById(R.id.card_view);

        this.addView(view);
    }

    @Override
    public void onSpringUpdate(Spring spring) {
        float value = (float) spring.getCurrentValue();

        middleView.setY(value);
    }

    @Override
    public void onSpringAtRest(Spring spring) {

    }

    @Override
    public void onSpringActivate(Spring spring) {

    }

    @Override
    public void onSpringEndStateChange(Spring spring) {

    }
}
