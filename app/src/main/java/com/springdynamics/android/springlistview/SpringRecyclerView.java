package com.springdynamics.android.springlistview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Stephen on 3/10/2015.
 */
public class SpringRecyclerView extends RecyclerView {
    public SpringRecyclerView(Context context) {
        super(context);
    }

    public SpringRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpringRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
