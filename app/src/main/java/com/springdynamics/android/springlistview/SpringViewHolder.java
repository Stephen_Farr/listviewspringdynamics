package com.springdynamics.android.springlistview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Stephen on 3/10/2015.
 */
public class SpringViewHolder extends RecyclerView.ViewHolder {
    public SpringViewHolder(View itemView) {
        super(itemView);
    }
}
