package com.springdynamics.android.springlistview;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringChain;
import com.facebook.rebound.SpringConfig;
import com.springdynamics.android.R;
import com.springdynamics.android.views.SpringView;

/**
 * Created by Stephen on 3/10/2015.
 */
public class SpringAdapter extends RecyclerView.Adapter<SpringViewHolder> {

    private static double TENSION = 800;
    private static double DAMPER = 20; //friction
    private SpringChain chain;

    public SpringAdapter(final SpringChain chain) {
        this.chain = chain;
    }

    @Override
    public SpringViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Resources r = viewGroup.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int)px);
        SpringView view = new SpringView(viewGroup.getContext());
        view.setLayoutParams(layoutParams);
        chain.addSpring(view);
        Spring spring = chain.getAllSprings().get(i);
        spring.setSpringConfig(new SpringConfig(TENSION, DAMPER));

        return new SpringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpringViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
